package hu.dpc.edu.entity;

import javax.persistence.*;

@Entity
public class Book extends EntityBase {
    @Id
    @GeneratedValue
    private Long id;

    @JoinColumn(name = "AUTHOR_ID")
    @ManyToOne(cascade = CascadeType.PERSIST)
    private Author author;

    private String title;

    public Book() {
    }

    public Book(Author author, String title) {
        this.author = author;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
