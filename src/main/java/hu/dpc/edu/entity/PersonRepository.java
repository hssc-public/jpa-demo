package hu.dpc.edu.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PersonRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public Person add(Person person) {
        entityManager.persist(person);
        return person;
    }

    @Transactional
    public List<Person> findAll() {
        return entityManager.createQuery("select p from Person p").getResultList();
    }

}
