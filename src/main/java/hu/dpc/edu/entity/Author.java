package hu.dpc.edu.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@DiscriminatorValue("A")
public class Author extends Person {
    @OneToMany(mappedBy = "author")
    private Set<Book>books = new HashSet<>();

    private String publisher;

    public Author() {
    }

    public Author(String name) {
        super(name);
    }

    public Set<Book> getBooks() {
        return books;
    }

    public void setBooks(Set<Book> books) {
        this.books = books;
    }

    @Override
    public String toString() {
        return "Author : " + getName() ;
    }
}
