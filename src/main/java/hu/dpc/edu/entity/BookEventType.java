package hu.dpc.edu.entity;

public enum BookEventType {
    CREATED,
    UPDATED
}
