package hu.dpc.edu.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class BookEvent {
    @Id
    @GeneratedValue
    private Long id;

    private String details;

    private BookEventType type;

    @Column(name = "HAPPENED_AT")
    private LocalDateTime happenedAt;

    public BookEvent() {
    }

    public BookEvent(String details, BookEventType type, LocalDateTime happenedAt) {
        this.details = details;
        this.type = type;
        this.happenedAt = happenedAt;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public BookEventType getType() {
        return type;
    }

    public void setType(BookEventType type) {
        this.type = type;
    }

    public LocalDateTime getHappenedAt() {
        return happenedAt;
    }

    public void setHappenedAt(LocalDateTime happenedAt) {
        this.happenedAt = happenedAt;
    }
}
