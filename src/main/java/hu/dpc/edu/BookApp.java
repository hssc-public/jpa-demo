package hu.dpc.edu;

import hu.dpc.edu.entity.Author;
import hu.dpc.edu.entity.Book;
import hu.dpc.edu.entity.Person;
import hu.dpc.edu.entity.PersonRepository;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class BookApp {
    public static void main(String[] args) {
        try(final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(PersistenceJPAConfig.class)) {
            final PersonRepository personRepository = context.getBean(PersonRepository.class);
            personRepository.add(new Person("VRG"));
            final BookRepository bookRepository = context.getBean(BookRepository.class);
            final Author jkRowling = new Author("J. K. Rowling");
            final Book chamberOfSecrets = new Book(jkRowling, "Harry Potter and the Chamber of Secrets");

            final Book book = bookRepository.add(chamberOfSecrets);

            System.out.println(personRepository.findAll());


        }
    }
}
