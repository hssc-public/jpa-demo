package hu.dpc.edu;

import hu.dpc.edu.entity.Book;
import hu.dpc.edu.entity.BookEvent;
import hu.dpc.edu.entity.BookEventType;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BookService {
    private BookRepository bookRepository;
    private BookEventRepository bookEventRepository;
    private CurrentTimeProvider currentTimeProvider;

    public BookService(BookRepository bookRepository, BookEventRepository bookEventRepository, CurrentTimeProvider currentTimeProvider) {
        this.bookRepository = bookRepository;
        this.bookEventRepository = bookEventRepository;
        this.currentTimeProvider = currentTimeProvider;
    }


    @Transactional
    public void add(Book book) {
        bookRepository.add(book);
        bookEventRepository.add(new BookEvent(book.toString(), BookEventType.CREATED, currentTimeProvider.getCurrentTime()));
    }

}
