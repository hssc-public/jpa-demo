package hu.dpc.edu;

import java.time.LocalDateTime;

public interface CurrentTimeProvider {

    LocalDateTime getCurrentTime();
}
