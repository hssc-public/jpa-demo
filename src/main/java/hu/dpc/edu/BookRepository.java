package hu.dpc.edu;

import hu.dpc.edu.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Set;

@Repository
public class BookRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional
    public Book add(Book book) {
        entityManager.persist(book);
        return book;
    }

    @Transactional
    public Book update(Book book) {
        final Book managed = entityManager.find(Book.class, book.getId());
        final boolean containsManaged = entityManager.contains(managed);
        final boolean containsBefore = entityManager.contains(book);
        final Book merged = entityManager.merge(book);
        final boolean containsAfter = entityManager.contains(book);
        final boolean containsMerged = entityManager.contains(merged);
//        merged.setTitle(merged.getTitle() + " HELLO");

        //entityManager.createQuery("select b.title from Book b").getResultList();
//        entityManager.flush();

    //    final String title = jdbcTemplate.queryForObject("select title from BOOK where id = ?", String.class, book.getId());
        return merged;
    }

    @Transactional
    public List<Book> findByAuthor(String author) {
        final List<Book> resultList = entityManager
                .createQuery("select b from Book b where b.author = :author", Book.class)
//                .setFirstResult(2)
//                .setMaxResults(1)
                .setParameter("author", author)
                .getResultList();
        return resultList;
    }

    @Transactional
    public Book delete(Book book) {
        final Book managed = entityManager.getReference(Book.class, book.getId());
        entityManager.remove(managed);
        return managed;
    }

    @Transactional(readOnly = true)
    public Book findById(long id) {
        final Book book = entityManager.find(Book.class, id);
        final Set<Book> books = book.getAuthor().getBooks();
        return book;
    }
}
